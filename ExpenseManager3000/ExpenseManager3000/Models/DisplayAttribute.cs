﻿using System;

namespace ExpenseManager3000.Models
{
    internal class DisplayAttribute : Attribute
    {
        public string Name { get; set; }
    }
}