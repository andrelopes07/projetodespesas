﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ExpenseManager3000.Models
{
    public class Despesa
    {
        public int DespesaId { get; set; }
        [DisplayName("Valor")]
        public decimal Valor { get; set; }
        [DisplayName("Descrição")]
        public string Descricao { get; set; }
        [DisplayName("Tipo de Despesa")]
        public int TipoDespesaId { get; set; }
        [DisplayName("Tipo de Despesa")]
        public virtual TipoDespesa TiposDespesa { get; set; }
        [DisplayName("Meio de Pagamento")]
        public int MeioPagamentoId { get; set; }
        [DisplayName("Meio de Pagamento")]
        public virtual MeioPagamento MeiosPagamento { get; set; }
        [DisplayName("Data")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Data { get; set; }
        [DisplayName("Comentários")]
        public string Comentarios { get; set; }
        public string UserId { get; set; }
    }
}