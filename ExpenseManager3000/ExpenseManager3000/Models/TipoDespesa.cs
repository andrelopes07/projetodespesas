﻿using System.ComponentModel;

namespace ExpenseManager3000.Models
{
    public class TipoDespesa
    {
        [DisplayName("Tipo de Despesa")]
        public int TipoDespesaId { get; set; }
        [DisplayName("Tipo de Despesa")]
        public string Descricao { get; set; }
        public string UserId { get; set; }

    }
}