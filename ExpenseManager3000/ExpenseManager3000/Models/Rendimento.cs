﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ExpenseManager3000.Models
{
    public class Rendimento
    {
        public int RendimentoId { get; set; }
        [DisplayName("Valor")]
        public decimal Valor { get; set; }
        [DisplayName("Descrição")]
        public string Descricao { get; set; }
        [DisplayName("Tipo de Rendimento")]
        public int TipoRendimentoId { get; set; }
        [DisplayName("Tipo de Rendimento")]
        public virtual TipoRendimento TiposRendimento { get; set; }
        [DisplayName("Data")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Data { get; set; }
        public string UserId { get; set; }
    }
}