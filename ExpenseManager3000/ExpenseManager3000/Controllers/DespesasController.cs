﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ExpenseManager3000.Models;
using Microsoft.AspNet.Identity;
using System;

namespace ExpenseManager3000.Controllers
{
    [Authorize]
    public class DespesasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Despesas
        public ActionResult Index(DateTime? inicio, DateTime? fim, int? mes)
        {
            var userId = User.Identity.GetUserId();
            var despesas = db.Despesas.Include(d => d.MeiosPagamento).Include(d => d.TiposDespesa).Where(d => d.UserId == userId);

            if (inicio != null && fim != null)
            {
                despesas = db.Despesas.Include(d => d.MeiosPagamento).Include(d => d.TiposDespesa).Where(d => d.UserId == userId).Where(d => d.Data >= inicio && d.Data <= fim);
            }

            if (mes != null)
            {
                despesas = db.Despesas.Include(d => d.MeiosPagamento).Include(d => d.TiposDespesa).Where(d => d.UserId == userId).Where(d => d.Data.Month == mes).OrderBy(d => d.TipoDespesaId);
            }

            return View(despesas.ToList());
        }

        // GET: Despesas/Create
        public ActionResult Create()
        {
            ViewBag.MeioPagamentoId = new SelectList(db.MeioPagamentoes.ToList().Where(m => m.UserId == User.Identity.GetUserId()), "MeioPagamentoId", "Descricao");
            ViewBag.TipoDespesaId = new SelectList(db.TipoDespesas.ToList().Where(t => t.UserId == User.Identity.GetUserId()), "TipoDespesaId", "Descricao");
            return View();
        }

        // POST: Despesas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DespesaId,Valor,Descricao,TipoDespesaId,MeioPagamentoId,Data,Comentarios,UserId")] Despesa despesa)
        {
            despesa.UserId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                db.Despesas.Add(despesa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MeioPagamentoId = new SelectList(db.MeioPagamentoes, "MeioPagamentoId", "Descricao", despesa.MeioPagamentoId);
            ViewBag.TipoDespesaId = new SelectList(db.TipoDespesas, "TipoDespesaId", "Descricao", despesa.TipoDespesaId);
            return View(despesa);
        }

        // GET: Despesas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Despesa despesa = db.Despesas.ToList().Where(m => m.UserId == User.Identity.GetUserId()).Where(m => m.DespesaId == id).SingleOrDefault();
            if (despesa == null)
            {
                return HttpNotFound();
            }
            ViewBag.MeioPagamentoId = new SelectList(db.MeioPagamentoes.ToList().Where(m => m.UserId == User.Identity.GetUserId()), "MeioPagamentoId", "Descricao", despesa.MeioPagamentoId);
            ViewBag.TipoDespesaId = new SelectList(db.TipoDespesas.ToList().Where(t => t.UserId == User.Identity.GetUserId()), "TipoDespesaId", "Descricao", despesa.TipoDespesaId);
            return View(despesa);
        }

        // POST: Despesas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DespesaId,Valor,Descricao,TipoDespesaId,MeioPagamentoId,Data,Comentarios,UserId")] Despesa despesa)
        {
            despesa.UserId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                db.Entry(despesa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MeioPagamentoId = new SelectList(db.MeioPagamentoes, "MeioPagamentoId", "Descricao", despesa.MeioPagamentoId);
            ViewBag.TipoDespesaId = new SelectList(db.TipoDespesas, "TipoDespesaId", "Descricao", despesa.TipoDespesaId);
            return View(despesa);
        }

        // GET: Despesas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Despesa despesa = db.Despesas.Find(id);
            if (despesa == null)
            {
                return HttpNotFound();
            }
            return View(despesa);
        }

        // POST: Despesas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Despesa despesa = db.Despesas.Find(id);
            db.Despesas.Remove(despesa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
