﻿using System.Web.Mvc;

namespace ExpenseManager3000.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}