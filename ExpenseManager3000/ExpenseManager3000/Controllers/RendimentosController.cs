﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ExpenseManager3000.Models;
using Microsoft.AspNet.Identity;
using System;

namespace ExpenseManager3000.Controllers
{
    [Authorize]
    public class RendimentosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Rendimentos
        public ActionResult Index(DateTime? inicio, DateTime? fim)
        {
            var userId = User.Identity.GetUserId();
            var rendimentos = db.Rendimentoes.Include(d => d.TiposRendimento).Where(d => d.UserId == userId);

            if (inicio != null && fim != null)
            {
                rendimentos = db.Rendimentoes.Include(d => d.TiposRendimento).Where(d => d.UserId == userId).Where(d => d.Data >= inicio && d.Data <= fim);
            }

            return View(rendimentos.ToList());
        }

        // GET: Rendimentos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rendimento rendimento = db.Rendimentoes.Find(id);
            if (rendimento == null)
            {
                return HttpNotFound();
            }
            return View(rendimento);
        }

        // GET: Rendimentos/Create
        public ActionResult Create()
        {
            ViewBag.TipoRendimentoId = new SelectList(db.TipoRendimentoes, "TipoRendimentoId", "Descricao");
            return View();
        }

        // POST: Rendimentos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RendimentoId,Valor,Descricao,TipoRendimentoId,Data,UserId")] Rendimento rendimento)
        {
            rendimento.UserId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                db.Rendimentoes.Add(rendimento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TipoRendimentoId = new SelectList(db.TipoRendimentoes, "TipoRendimentoId", "Descricao", rendimento.TipoRendimentoId);
            return View(rendimento);
        }

        // GET: Rendimentos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rendimento rendimento = db.Rendimentoes.Find(id);
            if (rendimento == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoRendimentoId = new SelectList(db.TipoRendimentoes, "TipoRendimentoId", "Descricao", rendimento.TipoRendimentoId);
            return View(rendimento);
        }

        // POST: Rendimentos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RendimentoId,Valor,Descricao,TipoRendimentoId,Data,UserId")] Rendimento rendimento)
        {
            rendimento.UserId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                db.Entry(rendimento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TipoRendimentoId = new SelectList(db.TipoRendimentoes, "TipoRendimentoId", "Descricao", rendimento.TipoRendimentoId);
            return View(rendimento);
        }

        // GET: Rendimentos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rendimento rendimento = db.Rendimentoes.Find(id);
            if (rendimento == null)
            {
                return HttpNotFound();
            }
            return View(rendimento);
        }

        // POST: Rendimentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rendimento rendimento = db.Rendimentoes.Find(id);
            db.Rendimentoes.Remove(rendimento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
