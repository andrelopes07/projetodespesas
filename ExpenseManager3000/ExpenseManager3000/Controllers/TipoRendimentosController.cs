﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ExpenseManager3000.Models;
using Microsoft.AspNet.Identity;

namespace ExpenseManager3000.Controllers
{
    [Authorize]
    public class TipoRendimentosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TipoRendimentos
        public ActionResult Index()
        {
            return View(db.TipoRendimentoes.ToList().Where(t => t.UserId == User.Identity.GetUserId()));
        }

        // GET: TipoRendimentos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoRendimentos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TipoRendimentoId,Descricao,UserId")] TipoRendimento tipoRendimento)
        {
            tipoRendimento.UserId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                db.TipoRendimentoes.Add(tipoRendimento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoRendimento);
        }

        // GET: TipoRendimentos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoRendimento tipoRendimento = db.TipoRendimentoes.Find(id);
            if (tipoRendimento == null)
            {
                return HttpNotFound();
            }
            return View(tipoRendimento);
        }

        // POST: TipoRendimentos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TipoRendimentoId,Descricao,UserId")] TipoRendimento tipoRendimento)
        {
            tipoRendimento.UserId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                db.Entry(tipoRendimento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoRendimento);
        }

        // GET: TipoRendimentos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoRendimento tipoRendimento = db.TipoRendimentoes.Find(id);
            if (tipoRendimento == null)
            {
                return HttpNotFound();
            }
            return View(tipoRendimento);
        }

        // POST: TipoRendimentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoRendimento tipoRendimento = db.TipoRendimentoes.Find(id);
            db.TipoRendimentoes.Remove(tipoRendimento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
