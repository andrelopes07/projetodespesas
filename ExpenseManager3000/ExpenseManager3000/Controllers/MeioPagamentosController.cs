﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExpenseManager3000.Models;
using Microsoft.AspNet.Identity;

namespace ExpenseManager3000.Controllers
{
    [Authorize]
    public class MeioPagamentosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: MeioPagamentos
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            return View(db.MeioPagamentoes.ToList().Where(m => m.UserId == userId));
        }

        // GET: MeioPagamentos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MeioPagamentos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MeioPagamentoId,Descricao,UserId")] MeioPagamento meioPagamento)
        {

            meioPagamento.UserId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                db.MeioPagamentoes.Add(meioPagamento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(meioPagamento);
        }

        // GET: MeioPagamentos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MeioPagamento meioPagamento = db.MeioPagamentoes.Find(id);
            if (meioPagamento == null)
            {
                return HttpNotFound();
            }
            return View(meioPagamento);
        }

        // POST: MeioPagamentos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MeioPagamentoId,Descricao,UserId")] MeioPagamento meioPagamento)
        {
            meioPagamento.UserId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                db.Entry(meioPagamento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(meioPagamento);
        }

        // GET: MeioPagamentos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MeioPagamento meioPagamento = db.MeioPagamentoes.Find(id);
            if (meioPagamento == null)
            {
                return HttpNotFound();
            }
            return View(meioPagamento);
        }

        // POST: MeioPagamentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MeioPagamento meioPagamento = db.MeioPagamentoes.Find(id);
            db.MeioPagamentoes.Remove(meioPagamento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
