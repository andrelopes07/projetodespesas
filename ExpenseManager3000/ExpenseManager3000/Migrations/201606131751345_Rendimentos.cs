namespace ExpenseManager3000.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Rendimentos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Rendimentoes",
                c => new
                    {
                        RendimentoId = c.Int(nullable: false, identity: true),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Descricao = c.String(),
                        TipoRendimentoId = c.Int(nullable: false),
                        Data = c.DateTime(nullable: false),
                        UserId = c.String(),
                    })
                .PrimaryKey(t => t.RendimentoId)
                .ForeignKey("dbo.TipoRendimentoes", t => t.TipoRendimentoId, cascadeDelete: true)
                .Index(t => t.TipoRendimentoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rendimentoes", "TipoRendimentoId", "dbo.TipoRendimentoes");
            DropIndex("dbo.Rendimentoes", new[] { "TipoRendimentoId" });
            DropTable("dbo.Rendimentoes");
        }
    }
}
