namespace ExpenseManager3000.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TipoRendimento : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TipoRendimentoes",
                c => new
                    {
                        TipoRendimentoId = c.Int(nullable: false, identity: true),
                        Descricao = c.String(),
                        UserId = c.String(),
                    })
                .PrimaryKey(t => t.TipoRendimentoId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TipoRendimentoes");
        }
    }
}
